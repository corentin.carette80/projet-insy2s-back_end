const router = require("express").Router();
const pdfController = require("../controllers/pdf.controller");

router.get("/product/:id", pdfController.printsPdf);

module.exports = router;
