const puppeteer = require("puppeteer");

generate = async (id) => {
  const browser = await puppeteer.launch();

  const page = await browser.newPage();

  await page.goto(`http://localhost:3000/product/${id}`);
  const pdf = await page.pdf({ format: "A4" });

  await browser.close();
  return pdf;
};

exports.printsPdf = (req, res) => {
  generate(req.params.id).then((pdf) => {
    res.set({
      "Content-Type": "application/pdf",
      "Content-Length": pdf.length,
    });
    res.send(pdf);
  });
};
